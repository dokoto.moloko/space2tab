#include "main.hpp"

int main(const int argc, const char* argv[])
{	
	#ifdef _MSC_VER
		std::locale::global(std::locale("spanish")); 
		std::wcout.imbue(std::locale("spanish"));	
	#endif
	#ifndef _MSC_VER
		std::locale::global(std::locale("spanish")); 
		std::cout.imbue(std::locale("es_ES.UTF-8"));	
	#endif	

	bool ret = cc::MainUnit::Run(argc, argv);
	#ifdef _MSC_VER
		std::cout << std::endl << "Pulse cualquier tecla para finalizar...";
		std::cin.get();
	#endif
	return ret;
}


bool cc::MainUnit::Run(CoInt_t argc, CoCStr_t argv[]) 
{ 
	if (cc::args::Check(argc, argv) == false) return false;	
	cc::ConvSp2Tab conv(cc::args::m_args[cc::args::mc_parm_in].c_str(), cc::args::m_args[cc::args::mc_parm_len].c_str(), cc::args::m_args[cc::args::mc_parm_out].c_str() );	
	
	if (conv.Conv() == false) return false;
	if (conv.Save() == false) return false;
	std::cout << "Fichero generado correctamente en : " << cc::args::m_args[cc::args::mc_parm_out] << std::endl;

	return true;
}
