#include "ConvSp2Tab.hpp"

namespace cc 
{
	ConvSp2Tab::ConvSp2Tab(CoCStr_t in_path, CoCStr_t lengs_path, CoCStr_t out_path) :
	m_in_path(in_path),
	m_out_path(out_path),
	m_lengs_path(lengs_path)

	{

	}

	ConvSp2Tab::~ConvSp2Tab(void)
	{
	}

	bool ConvSp2Tab::Conv()
	{
		if (loadLengs(m_lengs_path, m_lengs) == false)	return false;
		if (loadIn(m_in_path, m_file) == false)			return false;
		UInt_t pos = 0;
		for(File_t::iterator it = m_file.begin(); it != m_file.end(); it++)
		{
			pos = 0;
			size_t line_len = it->size();
			for(Lengs_t::iterator itl = m_lengs.begin(); itl != m_lengs.end()-1; itl++)
			{
				pos += (*itl);
				if (pos == 0)
				{
					std::cout << "ERROR: Una de las definiciones de tama�o del fichero de estructura es 0.";
					return false;
				}
				if ( line_len > pos)
					it->insert(pos, "\t");
				pos++;
			}
		}

		return true;
	}

	bool ConvSp2Tab::Save()
	{
		std::ofstream os(m_out_path);
		if (os.is_open())
		{
			for(File_t::iterator it = m_file.begin(); it != m_file.end(); it++)
				os << *it << std::endl;
			os.close();
		} 
		else
		{
			std::cerr << "Se ha producido un error al grabar el fichero resultado." << std::endl;			
			return false;
		}

		return true;
	}

	bool ConvSp2Tab::loadLengs(CoCStr_t lengs_path, Lengs_t& lengs)
	{
		std::ifstream is( lengs_path );
        if (is.is_open())
        {    
			Str_t line;
			UInt_t len = 0;
			while(is.good())
			{            
				getline(is, line);
				len = 0;				
				std::istringstream(line) >> len;				
				lengs.push_back(len);
			}
			is.close();		
			for (Lengs_t::iterator it = lengs.end()-1; it != lengs.begin(); it--)			
			{		
				if (*it > 0) break;
				if (*it == 0)
				{
					Lengs_t::iterator ita = it-1;
					lengs.erase(it);				
					it = ita;
				}
			}
		} 
		else
		{
			std::cerr << "Se ha producido un error al cargar el fichero de estructuras." << std::endl;
			return false;
		}

		return true;
	}

	bool ConvSp2Tab::loadIn(CoCStr_t in_path, File_t& file)
	{
		std::ifstream is( in_path );
        if (is.is_open())
        {            
			is.seekg(0, std::ios::beg);
			Buffer_t ss;
			ss << is.rdbuf();			
			is.close();
			if (ss.str().size() == 0) return false;			
			cc::utils::Split('\n', ss.str().c_str(), file);

		} 
		else
		{
			std::cerr << "Se ha producido un error al cargar el fichero de entrada." << std::endl;
			return false;
		}

		return true;
	}

}

