#include "args.hpp"

namespace cc 
{

args::Args_t args::m_args;
CoUInt_t args::m_oblig = 3;

Str_t cc::args::m_use = "\n\
Use: %s -in [\"Ruta Fichero Entrada\"] -len [\"Ruta al fichero de tama�os de campo\"] -out [\"Ruta Fichero Salida\"]\n\n\
\t'-len' : Es un fichero que contiene los tama�os de cada campo de los ficheros a comparar. Teniendo tantas definiciones\n\
\tde tama�o como campos tenga la estructura del ficheros.\n\
\t\tEJEMPLO DE FICHERO DE ESTRUCTURA SIN TRATAR:\n\
\t\t\tCAMPO1 CHAR 10\n\
\t\t\tCAMPO2 INT   4\n\
\t\t\tCAMPO3 CURR 16\n\
\t\tESTO SERIA LO QUE LE PASARIAMOS AL PARAMTRO '-len' :\n\
\t\t\t10\n\
\t\t\t4\n\
\t\t\t16\n\
\t\tAsi que su formato seria texto plano ASCI. Una sola columna con los tama�os de cada campo, un tama�o por linea.\n\n\
EJEMPLO DE LINEA DE COMANDO:\n\
Space2Tab -in \"C:\\DIR1\\Fichero.txt\" -len \"C:\\DIR1\\Fichero_lens.txt\" -out \"C:\\DIR1\\Fichero_Resultado.txt\"\n";

CoCStr_t args::mc_parm_in = "-in";
CoCStr_t args::mc_parm_out = "-out";
CoCStr_t args::mc_parm_len = "-len";
CoCStr_t args::mc_parm_prog = "prog";

	bool args::Check(CoInt_t argc, CoCStr_t argv[])
	{		
		if (load(argc, argv) == false) return false;
		UInt_t oblig = 0;
		for ( Args_t::iterator it = cc::args::m_args.begin(); it != cc::args::m_args.end(); it++)
		{
			if (((*it).first).compare(mc_parm_in) == 0)
			{
				if (valida1((*it).second) == false) return false;
				oblig++;         
			}
			else if (((*it).first).compare(mc_parm_out) == 0)        
			{
				oblig++;
			}
			else if (((*it).first).compare(mc_parm_len) == 0)        
			{
				if (valida1((*it).second) == false) return false;
				oblig++;
			}
			else if (((*it).first).compare(mc_parm_prog) == 0)
				continue;
			else
			{
				std::cerr <<  "Parametros no reconocido." << it->first << " Use --help." << std::endl;
				return false;
			}
		}

		if (oblig != cc::args::m_oblig)
		{
			std::cerr <<  "Faltan parametros obligatorios. Use --help." << std::endl;
			return false;
		}
		return true;
	}

	bool args::valida1(CoStr_t& valor)
	{
		std::ifstream inFile(valor.c_str());
		if (inFile.fail())
		{
			Buffer_t ss;
			ss << "El fichero no parece existir en : " << valor << std::endl;
			std::cerr << ss.str() << std::endl;
			return false;
		}
		inFile.close();

		return true;
	}

	Str_t args::GetProgName(CoCStr_t arg)
	{
		Str_t prog_name(arg);
		Str_t like_unix = prog_name.substr(0, 1);
		Str_t like_win  = prog_name.substr(1, 2);

		if ( like_unix.compare("/") == 0 )
			prog_name = prog_name.substr(prog_name.find_last_of("/")+1, prog_name.size());
		else if ( like_win.compare(":\\") == 0 ) 
			prog_name = prog_name.substr(prog_name.find_last_of("\\")+1, prog_name.size());

		return prog_name;
	}

	bool args::load(CoInt_t argc, CoCStr_t argv[])
	{
		
		size_t pos = 0;
		while ((pos = m_use.find("%s")) != Str_t::npos) m_use.replace(pos, 2, GetProgName(argv[0]));

		if (argc == 1)
		{
			std::cerr << m_use << std::endl;
			return false;
		}

		for (UInt_t i = 0; i < SC(size_t, argc-1);)
		{
			if (i == 0)
			{
				m_args[mc_parm_prog] = Str_t(argv[i]);
				i++;
			}
			else
			{
				m_args[Str_t(argv[i])] = Str_t(argv[i+1]);
				i += 2;
			}
		} 

		return true;
	}
}
